
/* Copyleft Anastasia R.
 * 
 * https://www.copyleft.org/
 */

/*
 Each sentence = 1 shape. 
 Generate different color and outline color for each sentence. 
 Start each shape at a different place.
 Each word is a line.
 Overall background color based on the whole text
 
 */

function ArtBot(customProps) {
    
    var bot = {
        properties: {
            getCanvas: function() {
                alert("No canvas to write to!");
                return null;
            },
            getText: function() {
                return "No text passed in";
            },
            onClearCanvas: function() {
                console.log("Clearing canvas");
                return;
            },
            onDrawShape: function(text, pointCoordinates) {
                console.log("Drawing shape for: '" + text + "' \n Point coordinates: ");
                console.log(pointCoordinates);
                return;
            },
            onGenerateArt: function() {
                console.log("Generating art");
                return;
            }
        },
        getCanvasContext: function() {
            var canvas = this.properties.getCanvas();
            var canvasContext = null;
            if (canvas.getContext) {
                canvasContext = canvas.getContext('2d');
            }
            return canvasContext;
        },


        getBackgroundColor: function(text) {
            var sumForText = this.getSumOfLetters(text).toString();
            var numOfDigitsPerRGB = sumForText.length / 3; //todo test strings less than 3 chars long
            var wildNum = this.getSumOfLetters(sumForText);
            var r = sumForText.substr(0, numOfDigitsPerRGB);
            var g = sumForText.substr(numOfDigitsPerRGB, numOfDigitsPerRGB);
            var b = sumForText.substr(numOfDigitsPerRGB * 2, numOfDigitsPerRGB);
            var maxColor = 255; // each of the r, g, and b components can only go up to 255 colors.

            // we don't want to have near-black backgrounds for all short messages.
            // multiply the values by a large number, then wrap it around 255 so that
            // it doesn't go over 255
            r = (r * wildNum) < maxColor ? (r * wildNum) : (r * wildNum) % maxColor;
            g = (g * wildNum) < maxColor ? (g * wildNum) : (g * wildNum) % maxColor;
            b = (b * wildNum) < maxColor ? (b * wildNum) : (b * wildNum) % maxColor;


            rgb = "rgb(" + r + "," + g + "," + b + ")";

            return rgb;
        },

        getSumOfLetters: function(text) {
            var sum = 0;

            if (text) {
                for (var i = 0; i < text.length; i++) {
                    sum += text.charCodeAt(i);
                }
            }

            return sum;
        },

        fillBg: function(text, canvasDom, canvasContext) {
            var canvasBgColor = this.getBackgroundColor(text);
            // fill out the background
            canvasContext.fillStyle = canvasBgColor;
            canvasContext.fillRect(0, 0, canvasDom.width, canvasDom.height);
        },

        drawShapes: function(text, canvasDom, canvasContext) {
            var sentences = text.split(/\.|\!|\?/g);

            for (var i in sentences) {
                this.drawShape(sentences[i], canvasDom, canvasContext);
            }
        },

        drawShape: function(text, canvasDom, canvasContext) {
            var xOffset = 0;
            var yOffset = 0;
            var offsetCoordiante;
            var words;
            var pointCoordinates = [];
            var fillColor;
            var outlineColor = "white";
            var coordinate;
            var maxXCoordinate = canvasDom.width;
            var maxYCoordinate = canvasDom.height;

            fillColor = this.getBackgroundColor(text);
            outlineColor = this.getBackgroundColor(this.reverse(text));

            // split on non-alphanumeric characters to get the array of words in a sentence
            words = text.split(/[\W_]+/g);
            pointCoordinates = this.getPointCoordinates(words);

            // figure out the offset of each shape so that they don't all
            // start at the same point.
            offsetCoordiante = this.getPointCoordinate(text);
            xOffset = this.coordinateWithingBounds(offsetCoordiante['x'], maxXCoordinate);
            yOffset = this.coordinateWithingBounds(offsetCoordiante['y'], maxYCoordinate);

            canvasContext.beginPath();
            canvasContext.moveTo(xOffset, yOffset);

            for (var i in pointCoordinates) {
                coordinate = pointCoordinates[i];
                canvasContext.lineTo(this.coordinateWithingBounds(xOffset + coordinate['x'], maxXCoordinate),
                        this.coordinateWithingBounds(yOffset + coordinate['y'], maxYCoordinate));
            }
            canvasContext.strokeStyle = outlineColor;
            canvasContext.fillStyle = fillColor;
            canvasContext.fill();
            canvasContext.stroke();
            
            this.properties.onDrawShape(text, pointCoordinates);
        },

        reverse: function(str) {
            return str.split("").reverse().join("");
        },

        coordinateWithingBounds: function(coordinate, maxCoordinate) {
            return coordinate < maxCoordinate ? coordinate : coordinate % maxCoordinate;
        },

        getPointCoordinates: function(words) {
            var coordinate;
            var coordinates = [];
            var text;

            for (var i in words) {
                text = words[i];
                coordinate = this.getPointCoordinate(text);

                coordinates.push(coordinate);
            }
            return coordinates;
        },

        getPointCoordinate: function(text) {
            var text;
            var half;
            var firstHalf;
            var secondHalf;
            var x;
            var y;

            half = text.length / 2;
            firstHalf = text.substr(0, half);
            secondHalf = text.substr(half, half);
            x = this.getSumOfLetters(firstHalf);
            y = this.getSumOfLetters(secondHalf);

            return {'x': x, 'y': y};
        },

        generateArt: function(textToUse) {
            var canvasDom = this.properties.getCanvas();;
            var text = textToUse || this.properties.getText();
            var canvasContext = this.getCanvasContext();

            this.fillBg(text, canvasDom, canvasContext);
            this.drawShapes(text, canvasDom, canvasContext);
            
            this.properties.onGenerateArt();
        },

        clearCanvas: function() {
            var canvasDom = this.properties.getCanvas();
            var canvasContext = this.getCanvasContext();

            canvasContext.clearRect(0, 0, canvasDom.width, canvasDom.height);
            this.properties.onClearCanvas();
        }
    };// end bot obj
    
    if(customProps)
        for(var i in customProps)
            bot.properties[i] = customProps[i];
    
    return bot;
    
}// end ArtBot class