
/*
 * This requires the OstrichRandomizer plugin.
 * 
 */

var languages = {
    english: {
        letters: {
            all: "abcdefghijklmnopqrstuvwxyz",
            consonants: "bcdfghjklmnpqrstuvwxyz",
            vowels: "aeuio"},
        singleLetterRealWords: ['I', 'a'],
        twoLetterRealWords: ["of", "to", "in", "it", "is", "be", "as", "at",
            "so", "we", "he", "by", "or", "on", "do", "if", "me", "my", "up",
            "an", "go", "no", "us", "am"],
        commonConsonantDigraphs: ["th", "ch", "ck", "sh", "qu", "wh", "ph", "ng"],
        commonVowelDigraphs: ["ai", "ay", "ea", "ee", "ei", "ey", "ie", "oa", "oo", "ow", "ue"],
        consonantLetterFrequencies: {
            2.70: 'b', 5.32: 'c', 6.52: 'd', 3.67: 'f', 3.52: 'g', 8.36: 'h',
            0.37: 'j', 1.36: 'k', 7.09: 'm', 12.08: 'n', 3.49: 'p', 0.17: 'q', 10.67: 'r',
            11.34: 's', 15.06: 't', 1.79: 'v', 3.08: 'w', 0.32: 'x', 2.90: 'y', 0.19: 'z'
        },
        vowelLetterFrequencies: {
            22.42: 'a', 31.73: 'e', 19.22: 'i', 19.59: 'o', 7.03: 'u'
        },
        allLetterFrequencies: {
            1.60: 'b', 3.16: 'c', 3.87: 'd',
            2.18: 'f', 2.09: 'g', 2.09: 'g', 4.96: 'h',
            0.22: 'j', 0.81: 'k', 4.21: 'm', 7.17: 'n',
            2.07: 'p', 0.10: 'q', 6.33: 'r', 6.73: 's', 8.94: 't',
            1.06: 'v', 1.83: 'w', 0.19: 'x', 1.72: 'y',
            0.11: 'z', 8.55: 'a', 12.10: 'e', 7.33: 'i', 7.47: 'o',
            2.68: 'u'
        },
        frequenciesPercision: 10
    }
};

function roboGhostBox(customProps) {

    var roboGhostBoxObj = {

        properties: {
            lengthOfWord: {min: 1, max: 8},
            lengthOfSentence: {min: 1, max: 15},
            numbers: "0123456789",
            speed: 1,
            turnedOn: false,
            language: languages.english,
            defaultLanguage: languages.english,

            // listeners
            onStart: function () {
                console.log("Robo Ghost Box Started");
            },
            onStop: function () {
                console.log("Robo Ghost Box Stopped");
            },
            onStopping: function () {
                console.log("Robo Ghost Box Stopping");
            },
            onSaySentence: function (text) {
                console.log(text);
            }
        },
        
        randomizer: OstrichRandomizer(),

        PRECEDING_STRING_ENUM: {NONE: 0, VOWEL: 1, CONS: 2, DOUBLE_VOWEL: 3, DOUBLE_CONS: 4},

        createPartitionArray: function (frequenciesArray) {
            var partitionArray = [];

            for (var i in frequenciesArray) {
                for (var j = 0; j < (i * this.properties.language.frequenciesPercision); j++) {
                    partitionArray.push(frequenciesArray[i]);
                }
            }
            return partitionArray;
        },
        loadData: function () {
            console.log('loading data');
            this.properties.language.vowelPartitionArray = this.createPartitionArray(this.properties.language.vowelLetterFrequencies);
            this.properties.language.consonantPartitionArray = this.createPartitionArray(this.properties.language.consonantLetterFrequencies);
            this.properties.language.allPartitionArray = this.properties.language.vowelPartitionArray.concat(this.properties.language.consonantPartitionArray);

        },

        startRoboGhostBox: function () {
            this.loadData();

            var text = this.getTextToSay();
            this.properties.turnedOn = true;
            var thisSavedRef = this;

            this.properties.onStart();

            // the reason this is a nested function is because we need to create
            // a closure to save the reference to this (as thisSavedRef). Otherwise,
            // when the function gets passed and assigned to another object for
            // execution, "this" refers to this new object, and we'll have no way of
            // accessing properties, etc.
            function speakNextSentence() {
                if (thisSavedRef.properties.turnedOn)
                    window.setTimeout(function () {
                        thisSavedRef.processText(thisSavedRef.getTextToSay(), speakNextSentence);
                    }, 1);
                else {
                    thisSavedRef.processText("End session.");
                    thisSavedRef.properties.onStop();
                }
            } // end speakNextSentenceClosure

            this.processText(text, speakNextSentence);
        },

        stopRoboGhostBox: function () {
            this.properties.turnedOn = false;

            this.properties.onStopping();
        },

        processText: function (text, doAfter) {
            this.properties.onSaySentence(text);
            textToSpeech(text, this.properties.speed, doAfter);
        },

        getTextToSay: function () {
            return this.getSentence(this.randomizer.getRandomNumber(this.properties.lengthOfSentence));
        },

        getSentence: function (lengthOfSentence) {
            var text = "";
            var lengthOfWord;

            for (var i = 0; i < lengthOfSentence; i++) {
                lengthOfWord = this.randomizer.getRandomNumber(this.properties.lengthOfWord);
                text += this.getWord(lengthOfWord);

                if (i !== lengthOfSentence - 1)
                    text += " ";
            }
            text += ".";
            return text;
        },

        getCharactersToChooseFrom: function (/* possible values: PRECEDING_STRING_ENUM */ precedingChars) {
            var chooseFrom = null;

            switch (precedingChars) {
                case this.PRECEDING_STRING_ENUM.NONE:
                    chooseFrom = this.properties.language.allPartitionArray;
                    break;
                case this.PRECEDING_STRING_ENUM.VOWEL:
                    chooseFrom = this.properties.language.allPartitionArray;
                    break;
                case this.PRECEDING_STRING_ENUM.CONS:
                    chooseFrom = this.properties.language.allPartitionArray;
                    break;
                case this.PRECEDING_STRING_ENUM.DOUBLE_CONS:
                    chooseFrom = this.properties.language.vowelPartitionArray;
                    break;
                case this.PRECEDING_STRING_ENUM.DOUBLE_VOWEL:
                    chooseFrom = this.properties.language.consonantPartitionArray;
                    break;
            }
            return chooseFrom;
        },

        isVowel: function (letter) {
            for (var i in this.properties.language.letters.vowels) {
                if (this.properties.language.letters.vowels[i] === letter)
                    return true;
            }
            for (var i in this.properties.language.commonVowelDigraphs) {
                if (this.properties.language.commonVowelDigraphs === letter)
                    return true;
            }
            return false;
        },

        getWord: function (lengthOfWord) {
            var result = '';
            var letterToAdd = null;
            var lastLetterAdded = this.PRECEDING_STRING_ENUM.NONE;
            var precedingStr = this.PRECEDING_STRING_ENUM.NONE;
            var addingVowel = false;

            if (lengthOfWord === 1) {
                result = this.randomizer.randArr(this.properties.defaultLanguage.singleLetterRealWords);
            } else if (lengthOfWord === 2) {
                result = this.randomizer.randArr(this.properties.defaultLanguage.twoLetterRealWords);
            } else
                for (var i = 0; i < lengthOfWord; i++) {
                    var chooseFrom = this.getCharactersToChooseFrom(precedingStr);
                    var length = chooseFrom.length;

                    letterToAdd = chooseFrom[this.randomizer.rand(0, length - 1)];
                    result += letterToAdd;
                    addingVowel = this.isVowel(letterToAdd);

                    if (letterToAdd === undefined)
                        return;

                    if (addingVowel && lastLetterAdded === this.PRECEDING_STRING_ENUM.VOWEL) {
                        precedingStr = this.PRECEDING_STRING_ENUM.DOUBLE_VOWEL;
                    } else if (!addingVowel && lastLetterAdded === this.PRECEDING_STRING_ENUM.CONS) {
                        precedingStr = this.PRECEDING_STRING_ENUM.DOUBLE_CONS;
                    } else if (addingVowel) {
                        precedingStr = this.PRECEDING_STRING_ENUM.VOWEL;
                    } else {
                        precedingStr = this.PRECEDING_STRING_ENUM.CONS;
                    }
                    lastLetterAdded = this.isVowel(letterToAdd) ? this.PRECEDING_STRING_ENUM.VOWEL : this.PRECEDING_STRING_ENUM.CONS;
                }
            return result;
        }
    };// end roboGhostBox object definition

    // set custom properties:
    if (customProps) {
        for (var i in customProps) {
            roboGhostBoxObj.properties[i] = customProps[i];
        }
    }

    return roboGhostBoxObj;
}// end roboGhostBox constructor