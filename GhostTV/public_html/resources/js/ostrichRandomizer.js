
function OstrichRandomizer(seedGenerator) {
    var ostrich = {
        
        
        rand: function (min, max) { // shortening
            return this.getRandomNumber({"min": min, "max": max});
        },

        
        getRandomNumber: function (range) {
            return Math.floor(Math.random() * range.max) + range.min;
        },

        randArr: function (arr) {
            return arr[this.getRandomNumber({min: 0, max: arr.length - 1})];
        },
    };
    
    return ostrich;
}