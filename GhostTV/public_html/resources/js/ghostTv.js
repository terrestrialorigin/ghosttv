var listeners = {
    onStart: function () {
        document.getElementById("started").style.display = "block";
        document.getElementById("stopping").style.display = "none";
        document.getElementById("stopped").style.display = "none";
    },
    onStop: function () {
        document.getElementById("started").style.display = "none";
        document.getElementById("stopping").style.display = "none";
        document.getElementById("stopped").style.display = "block";
    },
    onStopping: function () {
        document.getElementById("started").style.display = "none";
        document.getElementById("stopping").style.display = "block";
        document.getElementById("stopped").style.display = "none";
    },
    onSaySentence: function (text) {
        artBot.generateArt(text);

        document.getElementById("debugDisplay").innerHTML
                = document.getElementById("debugDisplay").innerHTML + text + "<br />";
    },

}
var ghostBox = roboGhostBox(listeners);

var props = {
    getCanvas: function () {
        return document.getElementById('canvas');
    },
    getText: function () {
        return document.getElementById('dialogInput').value;
    }
}
var artBot = ArtBot(props);